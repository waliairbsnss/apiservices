<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountyResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'counties',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'slug' => $this->slug
            ],
            'relationships' => [
                'province' => new ProvinceResource($this->whenLoaded('province'))
            ],
            'links' => [
                'self' => route('counties.show',['county' => $this->slug])
            ]
        ];
    }
}
