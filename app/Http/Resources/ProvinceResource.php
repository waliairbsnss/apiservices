<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProvinceResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'provinces',
            'id' => $this->id,
            'attributes' => [
                'name' => $this->name,
                'slug' => $this->slug
            ],
            'relationships' => [
                'counties' => CountyResource::collection($this->whenLoaded('counties'))
            ],
            'links' => [
                'self' => route('provinces.show',['province' => $this->slug])
            ],
        ];
    }
}
