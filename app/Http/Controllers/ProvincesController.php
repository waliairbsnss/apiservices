<?php

namespace App\Http\Controllers;

use App\Province;
use Carbon\Carbon;
use App\Http\Resources\ProvinceResource;
use Illuminate\Support\Facades\App;

class ProvincesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return $this->handleIndex(new Province, true, 'counties');
    }

    public function show($province)
    {
        $province = Province::whereSlug($province)->first();

        return $this->handleShow($province,'Province', 'counties');
    }
}
