<?php

namespace App\Http\Controllers;

use App\Http\Resources\CountyResource;
use App\Http\Resources\ProvinceResource;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function respondWithToken($token){
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ], 200);
    }

    protected function handleIndex(Model $model, $collection, $load = null){
        $data = $model->all();

        $data = $this->loader($data, $load);


        $method = $this->methodName(ucfirst(class_basename($model)));

        return $this->$method($collection, $data);
    }

    protected function handleShow($data, $alias, $load){
        if(is_null($data)){
            return response()->json([
                'status' => 'error',
                'message' => "Your {$alias} not found in our records.",
                'request_time' => Carbon::now()->unix()
            ],404);
        }
        $data = $this->loader($data, $load);

        $method = $this->methodName($alias);

        return $this->$method(false, $data);
    }

    protected function methodName($name)
    {
        return "charge".$name;
    }

    protected function loader($data, $relationship){
        if(request()->has("with_{$relationship}") && request()->get("with_{$relationship}") == 1){
            return $data->load($relationship);
        }
        return $data;
    }

    protected function chargeProvince($collection, $data)
    {
        return $collection ? ProvinceResource::collection($data) : new ProvinceResource($data);
    }

    protected function chargeCounty($collection, $data){
        return $collection ? CountyResource::collection($data) : new CountyResource($data);

    }

}
