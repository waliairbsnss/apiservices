<?php

namespace App\Http\Controllers;

use App\County;
use Carbon\Carbon;
use App\Http\Resources\CountyResource;

class CountiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return $this->handleIndex(new County, true, 'province');

    }

    public function show($county)
    {
        $county = County::whereSlug($county)->first();

        return $this->handleShow($county,'County', 'province');
    }
}
