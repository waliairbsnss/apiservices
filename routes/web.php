<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router){
    $router->post('login', 'AuthController@login');

    $router->group(['prefix' => 'provinces','as' => 'provinces'], function () use ($router){
        $router->get('/', [
            'as' => 'index', 'uses' => 'ProvincesController@index'
        ]);
        $router->get('/{province}', [
            'as' => 'show', 'uses' => 'ProvincesController@show'
        ]);
    });

    $router->group(['prefix' => 'counties','as' => 'counties'], function () use ($router){
        $router->get('/', [
            'as' => 'index', 'uses' => 'CountiesController@index'
        ]);
        $router->get('/{county}', [
            'as' => 'show', 'uses' => 'CountiesController@show'
        ]);
    });
});
